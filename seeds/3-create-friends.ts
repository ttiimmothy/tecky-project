import {Knex} from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Inserts seed entries
    await knex("friends").where("user_id", ">", "0").del();
    let kevin = (await knex.select("id").from("users").where("username", "kevin"))[0].id;
    let timothy = (await knex.select("id").from("users").where("username", "timothy"))[0].id;
    let jessica = (await knex.select("id").from("users").where("username", "jessica"))[0].id;
    let stephy = (await knex.select("id").from("users").where("username", "stephy"))[0].id;
    let shirley = (await knex.select("id").from("users").where("username", "shirley"))[0].id;
    await knex
        .insert([
            {
                user_id: kevin,
                friend_id: jessica,
            },
            {
                user_id: kevin,
                friend_id: stephy,
            },
            {
                user_id: kevin,
                friend_id: shirley,
            },
            {
                user_id: jessica,
                friend_id: kevin,
            },
            {
                user_id: stephy,
                friend_id: kevin,
            },
            {
                user_id: shirley,
                friend_id: kevin,
            },
            {
                user_id: timothy,
                friend_id: shirley,
            },
            {
                user_id: jessica,
                friend_id: timothy,
            },
        ])
        .into("friends");
}
