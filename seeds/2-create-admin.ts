import {Knex} from "knex";
import {hashPassword} from "../lib/hash";

export async function seed(knex: Knex): Promise<void> {
    // Inserts seed entries
    await knex("users").where("username", "admin@emoji").del();
    const users = await knex.select("*").from("users").where("username", "admin@emoji");
    if (users.length === 0) {
        const password = await hashPassword("adminemoji");
        await knex
            .insert({
                username: "admin@emoji",
                password: password,
                email: "emojichattecky@gmail.com",
                register_status: 2,
                uuid: "87654321Admin",
                display_name: "admin",
                icon: "admin.png",
                gender: 1,
                date_of_birth: "1996-12-28",
                orientation: 3,
                region: 6,
                height: 3,
                body_type: 2,
                job: 2,
                description: "I am handsome",
                is_vip: false,
                vip_dollar: 0,
            })
            .into("users");
    }
}
