export interface SocialUser {
    username: string;
    password: string;
}
export interface CustomFile {
    filename: string;
}

export interface User {
    id?: number;
    username: string;
    password: string;
    email: string;
    register_status: number;
    uuid: string;
    display_name: string;
    icon: string;
    gender: string;
    date_of_birth: Date;
    orientation: number;
    region: number;
    lat: number;
    lng: number;
    height: number;
    body_type: number;
    job: number;
    description: number;
    is_vip: boolean;
    vip_dollar: number;
    created_at: Date;
    updated_at: Date;
}

export interface InitUser {
    username: string;
    password: string;
    email: string;
}

export interface VerifyUser {
    username: string;
    uuid: string;
    display_name: string;
    gender: string;
    date_of_birth: Date;
    orientation: number;
    region: number;
    icon: string;
}

export interface Activities {
    host_id: number;
    activity_title: string;
    activity_content: string;
    approach_gender: string;
    activity_date: string;
    is_join: Boolean;
    created_at: Date;
    updated_at: Date;
}

export interface LatLon {
    id: number;
    display_name: string;
    icon: string;
    date_of_birth: Date;
    latitude: number;
    longitude: number;
}
