import nodemailer from "nodemailer";
import {knex} from "../main";
import {logger} from "./logger";

export async function sendVerifyEmail(username: string, email: string, uuid: string) {
    try {
        // create reusable transporter object using the default SMTP transport
        const transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 587,
            secure: false,
            auth: {
                user: process.env.GMAIL_USER,
                pass: process.env.GMAIL_PASS,
            },
        });
        let info;
        info = await transporter.sendMail({
            from: "emojichattecky@gmail.com", // sender address
            to: email, // list of receivers
            subject: " 確認emojiChat帳號 ✔", // Subject line
            text: `Dear ${username},\nClick the following link to verify your account\n${process.env.WEBSITE_LINK}/confirmregister.html?username=${username}&uuid=${uuid}\n`,
        });
        // send mail with defined transport object
        logger.debug(info);
    } catch (e) {
        logger.error(e);
    }
}

export async function sendForgetPasswordEmail(email: string) {
    try {
        let checkemail = await knex.select("*").from("users").where("email", email);
        let success = false;
        if (checkemail.length > 0) {
            // create reusable transporter object using the default SMTP transport
            const transporter = nodemailer.createTransport({
                host: "smtp.gmail.com",
                port: 587,
                secure: false,
                auth: {
                    user: process.env.GMAIL_USER,
                    pass: process.env.GMAIL_PASS,
                },
            });
            let info;
            info = await transporter.sendMail({
                from: "emojichattecky@gmail.com", // sender address
                to: email, // list of receivers
                subject: "重設密碼 ✔", // Subject line
                text: `Dear ${checkemail[0].username},\nClick the following link to reset your password\n${process.env.WEBSITE_LINK}/resetpassword.html?uuid=${checkemail[0].uuid}\n`,
            });
            // send mail with defined transport object
            logger.debug(info);
            success = true;
            return success;
        }
        return success;
    } catch (e) {
        logger.error(e);
        return;
    }
}
