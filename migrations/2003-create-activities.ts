import {Knex} from "knex";

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable("activities", (table) => {
        table.increments();
        table.integer("host_id").unsigned().notNullable();
        table.foreign("host_id").references("users.id");
        table.string("activity_title").notNullable();
        table.string("activity_content").notNullable();
        table.string("approach_gender").notNullable();
        table.date("activity_date").notNullable();
        table.integer("status");
        table.boolean("is_matched").defaultTo(false);
        table.boolean("is_completed").defaultTo(false);
        table.boolean("is_cancelled").defaultTo(false);
        table.timestamps(false, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable("activities");
}
