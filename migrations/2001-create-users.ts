import {Knex} from "knex";

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable("users", (table) => {
        table.increments();
        table.string("username").unique().notNullable();
        table.string("password").notNullable();
        table.string("email").unique();
        table.integer("register_status").notNullable().defaultTo(2).comment("1:toVerify, 2:register, 3:delete"); // 1=to verify, 2=registered, 3=delete
        table.string("uuid").unique().notNullable();
        table.string("display_name");
        table.string("icon");
        table.integer("gender").defaultTo(1);
        table.date("date_of_birth");
        table.integer("orientation").defaultTo(2);
        table.integer("region");
        table.double("latitude").defaultTo(0);
        table.double("longitude").defaultTo(0);
        table.integer("height").defaultTo(6); // 1=<140, 2=141-150, 3=151-160, 4=161-170
        table.integer("body_type").defaultTo(3); // 1=肥胖, 2=微胖, 3=瘦長, 11=嬌小
        table.integer("job");
        table.string("description").defaultTo("hello");
        table.boolean("is_vip").notNullable().defaultTo(false);
        table.integer("vip_dollar").notNullable().defaultTo(0);
        table.timestamps(false, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable("users");
}
