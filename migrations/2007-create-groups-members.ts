import {Knex} from "knex";

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable("groups_members", (table) => {
        table.increments();
        table.integer("group_id").unsigned();
        table.foreign("group_id").references("groups.id");
        table.integer("member_id").unsigned();
        table.foreign("member_id").references("users.id");
        table.boolean("is_admin").notNullable().defaultTo(false);
		table.unique(["group_id", "member_id"]);
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable("groups_members");
}
