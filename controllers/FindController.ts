import {Request, Response} from "express";
import {logger} from "../lib/logger";
import {LatLon} from "../lib/model";
import {getDistanceFromLatLonInKm} from "../lib/shortestDistanceOnMap";
import {FindService} from "../services/FindService";

export class FindController {
    constructor(private findService: FindService) {}

    showAvailable = async (req: Request, res: Response) => {
        try {
            let user = (await this.findService.getUser(req.session["user"].id))[0]
            logger.debug(JSON.stringify(user, null, 4));
            let availableUser = await this.findService.availableUser(user);
            res.json(availableUser);
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    currentLocation = async (req: Request, res: Response) => {
        try {
            const id = req.session["user"].id;
            let {lat, lng} = req.body;
            let {latitude, longitude} = (await this.findService.originalLocation(id))[0];
            let distance = getDistanceFromLatLonInKm(lat, lng, latitude, longitude);
            if (distance > 0.1) {
                await this.findService.currentLocation(id, lat, lng);
            }
            res.json({success: true});
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    getOtherUsersLatLon = async (req: Request, res: Response) => {
        try {
            let user = (await this.findService.getUser(req.session["user"].id))[0]
            let otherUsersLocation: LatLon[] = await this.findService.availableUser(user);
            let selfLocation = (await this.findService.originalLocation(user.id))[0];
            let points = otherUsersLocation.filter((userLocation) => {
                return (
                    getDistanceFromLatLonInKm(
                        selfLocation.latitude,
                        selfLocation.longitude,
                        userLocation.latitude,
                        userLocation.longitude
                    ) < 1
                );
            });
            logger.debug(JSON.stringify(points, null, 4));
            res.json(points);
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };
}
