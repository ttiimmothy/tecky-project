import fetch from "node-fetch";
import crypto from "crypto";
import {Request, Response} from "express";
import {UserService} from "../services/UserService";
import {InitUser, VerifyUser} from "../lib/model";
import {checkPassword, hashPassword} from "../lib/hash";
import {uuidGenerator} from "../lib/uuid";
import {logger} from "../lib/logger";
import {sendForgetPasswordEmail, sendVerifyEmail} from "../lib/sendEmail";
import {createResponse, my_register_status} from "../lib/response";
const oauthSignature = require("oauth-signature");

export class UserController {
    constructor(private userService: UserService) {}

    login = async (req: Request, res: Response) => {
        try {
            const {username, password} = req.body;
            logger.debug(username);

            const userFound = (await this.userService.login(username))[0];

            if (userFound) {
                const match = await checkPassword(password, userFound.password);
                if (match) {
                    req.session["user"] = userFound;
                    logger.debug(JSON.stringify(req.session["user"], null, 4));
                    logger.debug(typeof userFound.id);
                    res.json({success: true});
                } else {
                    req.session["user"] = null;
                    res.status(302).json({success: false});
                }
            }
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    sendVerify = async (req: Request, res: Response) => {
        try {
            const user: InitUser = req.body;
            let checkUsername = await this.userService.getUser(user.username);
            let checkEmail = await this.userService.getEmail(user.email);
            logger.debug(checkUsername);

            if (checkUsername.length > 0) {
                // *****
                res.status(400).json(createResponse(false, my_register_status.used_user, "used username"));
                // *****
                return;
            }
            if (checkEmail.length > 0) {
                res.status(400).json(createResponse(false, my_register_status.used_email, "used username"));
                return;
            }

            const hashedPassword = await hashPassword(user.password);
            const uuid = await uuidGenerator();
            let id = await this.userService.createUser(user.username, hashedPassword, user.email, uuid);
            logger.debug(typeof id);
            logger.debug(id[0]);
            await sendVerifyEmail(req.body.username, req.body.email, uuid);
            res.json({success: true});
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    completeRegister = async (req: Request, res: Response) => {
        try {
            logger.debug(req.file);
            const user: VerifyUser = req.body;
            if (req.file) {
                user.icon = req.file.filename;
            }
            user.uuid = await uuidGenerator();
            await this.userService.verifyUser(user);
            res.json({success: true});
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    checkUuid = async (req: Request, res: Response) => {
        try {
            const uuidInfo: VerifyUser = req.body;
            let checkUuid = await this.userService.checkUuid(uuidInfo);
            if (checkUuid.length === 0) {
                logger.debug("404 Not Found");
                res.status(404).json({success: false});
                return;
            }
            res.json({success: true});
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    loginGoogle = async (req: Request, res: Response) => {
        try {
            const accessToken = req.session["grant"].response.access_token;
            logger.debug(accessToken);
            const fetchRes = await fetch("https://www.googleapis.com/oauth2/v2/userinfo", {
                method: "get",
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                },
            });
            const result = await fetchRes.json();
            let user = (await this.userService.socialLogin(result.email))[0];
            logger.debug(user);
            if (!user) {
                let newUser = {
                    username: result.email,
                    password: await hashPassword(crypto.randomBytes(30).toString()),
                };
                const userId = await this.userService.newSocialLogin(newUser.username, newUser.password, await uuidGenerator());
                newUser["id"] = userId[0]
				req.session["user"] = newUser;
            } else {
                req.session["user"] = user;
            }
            res.redirect("/find.html");
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    loginTwitter = async (req: Request, res: Response) => {
        try {
            logger.debug(req.session["grant"]);
            const accessToken = req.session["grant"].response.access_token;
            const accessSecret = req.session["grant"].response.access_secret;
            logger.debug(accessToken);
            logger.debug(accessSecret);

            const httpMethod = "get";
            const url = "https://api.twitter.com/1.1/account/verify_credentials.json";
            const timestamp = new Date().getTime() / 1000; // in seconds
            const parameters = {
                oauth_consumer_key: process.env.TWITTER_CLIENT_ID,
                oauth_token: accessToken,
                oauth_nonce: "kllo9940pd9333jh",
                oauth_timestamp: timestamp,
                oauth_signature_method: "HMAC-SHA1",
                oauth_version: "1.0",
                include_email: true,
            };
            const consumerSecret = process.env.TWITTER_CLIENT_SECRET;
            const signature = oauthSignature.generate(httpMethod, url, parameters, consumerSecret, accessSecret);
            logger.debug(signature);
            const fetchRes = await fetch(
                `https://api.twitter.com/1.1/account/verify_credentials.json?include_email=true&oauth_consumer_key=${process.env.TWITTER_CLIENT_ID}&oauth_nonce=kllo9940pd9333jh&oauth_signature_method=HMAC-SHA1&oauth_token=${accessToken}&oauth_timestamp=${timestamp}&oauth_version=1.0&oauth_signature=${signature}`
            );
            const result = await fetchRes.json();
            logger.debug(fetchRes);
            logger.debug(result);

            let user = (await this.userService.socialLogin(result.screen_name))[0];
            logger.debug(user);

            if (!user) {
                let newUser = {
                    username: result.screen_name,
                    password: await hashPassword(crypto.randomBytes(30).toString()),
                };

                const userId = await this.userService.newSocialLogin(newUser.username, newUser.password, await uuidGenerator());
                newUser["id"] = userId[0]
				req.session["user"] = newUser;
            } else {
                req.session["user"] = user;
            }
            res.redirect("/find.html");
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    loginFacebook = async (req: Request, res: Response) => {
        try {
            const accessToken = req.session["grant"].response.access_token;
            logger.debug(accessToken);
            const fetchRes = await fetch(`https://graph.facebook.com/me?access_token=${accessToken}`, {
                method: "get",
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                },
            });
            const result = await fetchRes.json();
            logger.debug(JSON.stringify(result, null, 4));
            logger.debug(result.name);

            let user = (await this.userService.socialLogin(result.name))[0];
            logger.debug(JSON.stringify(user, null, 4));

            if (!user) {
                let newUser = {
                    username: result.name,
                    password: await hashPassword(crypto.randomBytes(30).toString()),
                };
                const userId = await this.userService.newSocialLogin(newUser.username, newUser.password, await uuidGenerator());
				newUser["id"] = userId[0]
                req.session["user"] = newUser;
            } else {
                req.session["user"] = user;
            }
            res.redirect("/find.html");
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    forgetPassword = async (req: Request, res: Response) => {
        try {
            let success = await sendForgetPasswordEmail(req.body.email);
            res.json({success: success});
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    checkEmail = async (req: Request, res: Response) => {
        try {
            let checkEmail = await this.userService.getEmail(req.body.email);
            const success = checkEmail.length > 0;
            res.json({success});
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    getUsernameResetPassword = async (req: Request, res: Response) => {
        try {
            const username = (await this.userService.getUsername(req.body.uuid))[0].username;
            if (username) {
                res.json({success: true, username: username});
            } else {
                res.json({success: false});
            }
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    getAllUsers = async (req: Request, res: Response) => {
        try {
            const loginInfos = await this.userService.getAllUsers();
            res.json(loginInfos);
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    resetPassword = async (req: Request, res: Response) => {
        try {
            let {name, password} = req.body;
            let hashedPassword = await hashPassword(password);
            let uuid = await uuidGenerator();
            logger.debug(password);
            logger.debug(uuid);

            await this.userService.resetPassword(name, hashedPassword, uuid);
            res.json({success: true});
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    logout = async (req: Request, res: Response) => {
        try {
            delete req.session["user"];
            res.redirect("/");
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    currentUser = async (req: Request, res: Response) => {
        try {
            if (req.session["user"]) {
                res.json({success: true, user: req.session["user"]});
            } else {
                res.json({success: false});
            }
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };
}
