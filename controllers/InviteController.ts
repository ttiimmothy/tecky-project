import {Request, Response} from "express";
import {logger} from "../lib/logger";
import {InviteService} from "../services/InviteService";

export class InviteController {
    constructor(private inviteService: InviteService) {}

    showInviteReceived = async (req: Request, res: Response) => {
        try {
            let userID = req.session["user"].id;
            let inviteSent = await this.inviteService.received(userID);
            res.json(inviteSent);
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    showInviteSent = async (req: Request, res: Response) => {
        try {
            let userID = req.session["user"].id;
            let inviteSent = await this.inviteService.sent(userID);
            res.json(inviteSent);
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };
}
