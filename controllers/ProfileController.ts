import {Request, Response} from "express";
import {hashPassword} from "../lib/hash";
import {logger} from "../lib/logger";
import {ProfileService} from "../services/ProfileService";

export class ProfileController {
    constructor(private profileService: ProfileService) {}

    getProfile = async (req: Request, res: Response) => {
        try {
            logger.debug("getFriendList");
            let id = req.session["user"].id;
            let userProfile = await this.profileService.userProfile(id);
            res.json(userProfile[0]);
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    hobbyList = async (req: Request, res: Response) => {
        try {
            let id = req.session["user"].id;
            let userProfile = await this.profileService.hobbyList(id);
            res.json(userProfile);
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    updateProfileBasic = async (req: Request, res: Response) => {
        try {
            logger.debug("1");
            logger.debug(JSON.stringify(req.session["user"], null, 4));
            let user = req.body;
            user.id = req.session["user"].id;
            logger.debug("2");
            if (req.file) {
                user.icon = req.file.filename;
                req.session["user"].icon = user.icon;
            }
            logger.debug("3");
            if (user.password) {
                let updatePassword = user.password;
                user.password = await hashPassword(updatePassword);
                req.session["user"].password = user.password;
            }

            req.session["user"].display_name = user.display_name;
            req.session["user"].description = user.description;
            req.session["user"].orientation = parseInt(user.orientation);
            logger.debug(user.orientation);
            logger.debug(typeof user.orientation);
            req.session["user"].region = parseInt(user.region);
            req.session["user"].height = parseInt(user.height);
            req.session["user"].body_type = parseInt(user.body_type);
            req.session["user"].job = parseInt(user.job);
			// logger.info(user) causes error
            await this.profileService.updateProfileBasic(user);
            res.json({success: true});
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    updateProfileHobby = async (req: Request, res: Response) => {
        try {
            let hobbies = req.body;
            logger.debug(JSON.stringify(hobbies, null, 4));
            let hobbyUser_id = req.session["user"].id;
            await this.profileService.updateProfileHobby(hobbies.hobby, hobbyUser_id);
            res.json({success: true});
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };
}
