import {Knex} from "knex";
import {Activities} from "../lib/model";

export class ActivityService {
    constructor(private knex: Knex) {}

    async addNewActivity(activities: Activities) {
        let activity_id = parseInt(await this.knex.insert(activities).into("activities").returning("id"));
        await this.knex
            .insert({
                activity_id: activity_id,
                participant_id: activities.host_id,
                is_host: true,
            })
            .into("activities_participants");
        return;
    }

    async joinActivity(activityId: number, user_id: number) {
        await this.knex
            .insert({
                activity_id: activityId,
                participant_id: user_id,
                is_host: false,
            })
            .into("activities_participants");
    }

    async confirmApplication(user_id: number, activityId: number) {
        await this.knex("activities_participants").update({is_approved: true}).where({
            participant_id: user_id,
            activity_id: activityId,
        });
        await this.knex("activities").update({is_matched: true}).where({
            id: activityId,
        });
    }
    async completeActivity(activityId: number) {
        await this.knex("activities").update({is_completed: true}).where({
            id: activityId,
        });
    }
    async cancelActivities(activityId: number) {
        await this.knex("activities").update({is_cancelled: true}).where({
            id: activityId,
        });
    }

    async appointedActivity(user_id: number) {
        const txn = await this.knex.transaction();
        try {
            let joinedActivity: Array<number> = await txn
                .select("activity_id")
                .from("activities_participants")
                .andWhere((event) =>
                    event
                        .where({
                            participant_id: user_id,
                            is_approved: true,
                            is_quit: false,
                        })
                        .orWhere({
                            participant_id: user_id,
                            is_host: true,
                            is_quit: false,
                        })
                )
                .pluck("activity_id");

            if (!joinedActivity[0]) {
                return [[], null];
            } else {
                let activity = await txn.raw(/*sql*/ `
					select activities.id as activities_id,
					activities.host_id,
					activity_title,
					activity_content,
					approach_gender,
					activity_date,
					is_matched,
					is_completed,
					is_cancelled,
					activities.created_at as activities_created_at,
					activities.updated_at as activities_updated_at,
					json_agg(participant_id) as participant_id,
					json_agg(display_name) as display_name,
					json_agg(icon) as icon,
					json_agg(gender) as gender,
					json_agg(date_of_birth) as date_of_birth,
					json_agg(description) as description
				from activities
					left outer join activities_participants on activities_participants.activity_id = activities.id
					left outer join users on activities_participants.participant_id = users.id
				where activities.id in (${joinedActivity}) and is_matched = true and is_completed = false and is_cancelled = false and (is_approved=true or is_host= true)
				group by activities_id,
					activities.host_id,
					activity_title,
					activity_content,
					approach_gender,
					activity_date,
					is_matched,
					is_completed,
					is_cancelled,
					activities_created_at,
					activities_updated_at;`);
                await txn.commit();
                return [activity.rows, user_id];
            }
        } catch (e) {
            await txn.rollback();
            throw e;
        }
    }

    async toConfirmActivityList(user_id: number) {
        const txn = await this.knex.transaction();
        try {
            let joinedActivity: Array<number> = await txn
                .select("activity_id")
                .from("activities_participants")
                .where({
                    participant_id: user_id,
                    is_host: true,
                })
                .pluck("activity_id");

            if (!joinedActivity[0]) {
                return [];
            } else {
                let activity = await txn.raw(/*sql*/ `
				select activities.id as activities_id,
					activities.host_id,
					activity_title,
					activity_content,
					approach_gender,
					activity_date,
					is_matched,
					is_completed,
					is_cancelled,
					activities.created_at as activities_created_at,
					activities.updated_at as activities_updated_at,
					json_agg(participant_id) as participant_id,
					json_agg(display_name) as display_name,
					json_agg(icon) as icon,
					json_agg(gender) as gender,
					json_agg(date_of_birth) as date_of_birth,
					json_agg(description) as description
				from activities
					left outer join activities_participants on activities_participants.activity_id = activities.id
					left outer join users on activities_participants.participant_id = users.id
				where activities.id in (${joinedActivity}) and is_matched = false and is_completed = false and is_cancelled = false
				group by activities_id,
					activities.host_id,
					activity_title,
					activity_content,
					approach_gender,
					activity_date,
					is_matched,
					is_completed,
					is_cancelled,
					activities_created_at,
					activities_updated_at;`);
                await txn.commit();
                return activity.rows;
            }
        } catch (e) {
            await txn.rollback();
            throw e;
        }
    }

    async waitingActivity(user_id: number) {
        const txn = await this.knex.transaction();
        try {
            let appliedActivity: Array<number> = await txn
                .select("activity_id")
                .from("activities_participants")
                .where({
                    participant_id: user_id,
                    is_host: false,
                    is_approved: false,
                    is_quit: false,
                })
                .pluck("activity_id");

            const result = await txn
                .select("*", "activities.id as activities_id")
                .from("activities")
                .whereIn("activities.id", appliedActivity)
                .where({
                    is_matched: false,
                    is_completed: false,
                    is_cancelled: false,
                })
                .whereNot("host_id", user_id)
                .leftOuterJoin("users", "users.id", "activities.host_id");
            await txn.commit();
            return result;
        } catch (e) {
            await txn.rollback();
            throw e;
        }
    }

    async activityList(user_id: number, user_gender: number) {
        let joinedActivity: Array<number> = await this.knex
            .select("activity_id")
            .from("activities_participants")
            .where("participant_id", user_id)
            .pluck("activity_id");

        return await this.knex
            .select("*", "activities.id as activities_id")
            .from("activities")
            .whereNotIn("activities.id", joinedActivity)
            .andWhere((event) =>
                event
                    .where({
                        approach_gender: user_gender,
                        is_matched: false,
                        is_completed: false,
                        is_cancelled: false,
                    })
                    .orWhere({
                        approach_gender: 9,
                        is_matched: false,
                        is_completed: false,
                        is_cancelled: false,
                    })
            )
            .whereNot("host_id", user_id)
            .leftOuterJoin("users", "users.id", "activities.host_id");
    }

    async pastActivityList(user_id: number) {
        let joinedActivity: Array<number> = await this.knex
            .select("activity_id")
            .from("activities_participants")
            .where("participant_id", user_id)
            .pluck("activity_id");

        if (!joinedActivity[0]) {
            return [[], [], null];
        } else {
            let successActivity = await this.knex.raw(/*sql*/ `
				select activities.id as activities_id,
				activities.host_id,
				activity_title,
				activity_content,
				approach_gender,
				activity_date,
				is_matched,
				is_completed,
				is_cancelled,
				activities.created_at as activities_created_at,
				activities.updated_at as activities_updated_at,
				json_agg(participant_id) as participant_id,
				json_agg(display_name) as display_name,
				json_agg(icon) as icon,
				json_agg(gender) as gender,
				json_agg(date_of_birth) as date_of_birth,
				json_agg(description) as description
			from activities
				left outer join activities_participants on activities_participants.activity_id = activities.id
				left outer join users on activities_participants.participant_id = users.id
			where activities.id in (${joinedActivity}) and is_matched = true and is_completed = true and is_cancelled = false and (is_approved=true or is_host= true)
			group by activities_id,
				activities.host_id,
				activity_title,
				activity_content,
				approach_gender,
				activity_date,
				is_matched,
				is_completed,
				is_cancelled,
				activities_created_at,
				activities_updated_at;`);
            let failActivity = await this.knex.raw(/*sql*/ `
				select activities.id as activities_id,
				activities.host_id,
				activity_title,
				activity_content,
				approach_gender,
				activity_date,
				is_matched,
				is_completed,
				is_cancelled,
				activities.created_at as activities_created_at,
				activities.updated_at as activities_updated_at,
				json_agg(participant_id) as participant_id,
				json_agg(display_name) as display_name,
				json_agg(icon) as icon,
				json_agg(gender) as gender,
				json_agg(date_of_birth) as date_of_birth,
				json_agg(description) as description
			from activities
				left outer join activities_participants on activities_participants.activity_id = activities.id
				left outer join users on activities_participants.participant_id = users.id
			where activities.id in (${joinedActivity}) and ((is_matched = true and is_approved=false and is_host= false) or is_cancelled = true or is_quit) and (participant_id = ${user_id})
			group by activities_id,
				activities.host_id,
				activity_title,
				activity_content,
				approach_gender,
				activity_date,
				is_matched,
				is_completed,
				is_cancelled,
				activities_created_at,
				activities_updated_at;`);
            return [successActivity.rows, failActivity.rows, user_id];
        }
    }

    async newActivityMessage(activity_id: number, participant_id: number, content: string) {
        await this.knex
            .insert({activity_id: activity_id, participant_id: participant_id, content: content})
            .into("activities_messages");
    }

    async showChatUsers(userId: number) {
        return await this.knex
            .select("username", "description", "display_name", "icon")
            .from("users")
            .where("id", userId);
    }

    async activityChat(activityId: number) {
        return await this.knex
            .select("*")
            .from("activities_messages")
            .where("activity_id", activityId)
            .orderBy("created_at");
    }
}
