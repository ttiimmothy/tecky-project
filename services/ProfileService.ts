import {Knex} from "knex";
import {User} from "../lib/model";

export class ProfileService {
    constructor(private knex: Knex) {}

    async userProfile(id: number) {
        return await this.knex.select("*").from("users").where("id", id);
    }

    async hobbyList(id: number) {
        return await this.knex.select("hobby_id").from("hobbies_relations").where("user_id", id);
    }

    async updateProfileBasic(user: User) {
        await this.knex("users").update(user).where("id", user.id);
    }

    async updateProfileHobby(hobbies: number[], hobbyUser_id: number) {
        await this.knex("hobbies_relations").where("user_id", hobbyUser_id).del();
        for (let hobby of hobbies) {
            await this.knex.insert({user_id: hobbyUser_id, hobby_id: hobby}).into("hobbies_relations");
        }
    }
}
