import {Knex} from "knex";

export class GroupService {
    constructor(private knex: Knex) {}

    async addGroup(name: string, text: string, icon: string, userId: number) {
        let groupId = await this.knex
            .insert({group_name: name, description: text, group_icon: icon})
            .into("groups")
            .returning("id");
        await this.knex.insert({group_id: groupId[0], member_id: userId, is_admin: true}).into("groups_members");
    }

    async groupList() {
        return await this.knex.select("*").from("groups");
    }

    async groupWaitInfoList(id: number) {
        return await this.knex
            .select("group_id")
            .from("users")
            .innerJoin("groups_waitmembers", "member_id", "users.id")
            .where("member_id", id);
    }

    async checkInGroup(id: number) {
        return await this.knex.select("*").from("groups_members").where("member_id", id);
    }

    async checkInSpecificGroup(member_id: number, group_id: number) {
        return await this.knex
            .select("*")
            .from("groups_members")
            .where("member_id", member_id)
            .andWhere("group_id", group_id);
    }

    async joinGroup(group_id: number, member_id: number) {
        await this.knex("groups_waitmembers").where({group_id: group_id, member_id: member_id}).del();
        return await this.knex.insert({group_id: group_id, member_id: member_id}).into("groups_waitmembers");
    }

    async groupName(id: number) {
        return await this.knex.select("group_name", "description", "group_icon").from("groups").where("id", id);
    }

    async groupInfo(id: number) {
        return await this.knex
            .select("member_id", "icon", "display_name", "is_admin")
            .from("users")
            .innerJoin("groups_members", "member_id", "users.id")
            .where("group_id", id);
    }

    async groupWaitInfo(id: number) {
        return await this.knex
            .select("users.id as userId", "icon", "display_name")
            .from("users")
            .innerJoin("groups_waitmembers", "member_id", "users.id")
            .where("group_id", id);
    }

    async adminInfo(member_id: number, group_id: number) {
        return await this.knex
            .select("is_admin")
            .from("users")
            .innerJoin("groups_members", "member_id", "users.id")
            .where("group_id", group_id)
            .andWhere("member_id", member_id);
    }

    async getGroupChat(id: number) {
        return await this.knex
            .select("icon", "users.id as userId", "display_name", "member_id", "content", "groups_messages.created_at")
            .from("users")
            .innerJoin("groups_messages", "member_id", "users.id")
            .where("group_id", id)
            .orderBy("groups_messages.created_at");
    }

    async checkAdmin(group_id: number, member_id: number) {
        return await this.knex
            .select("is_admin")
            .from("groups_members")
            .where("group_id", group_id)
            .andWhere("member_id", member_id);
    }

    async addAdmin(group_id: number, adminArray: Array<number>) {
        for (let admin of adminArray) {
            await this.knex("groups_members")
                .update({is_admin: true})
                .where("group_id", group_id)
                .andWhere("member_id", admin);
        }
    }

    async addToGroup(group_id: number, member_id: number) {
        await this.knex("groups_waitmembers").where({group_id: group_id, member_id: member_id}).del();
        await this.knex("groups_members").where({group_id: group_id, member_id: member_id}).del();
        await this.knex.insert({group_id: group_id, member_id: member_id}).into("groups_members");
    }

    async changeGroup(name: string, text: string, icon: string, group_id: number) {
        await this.knex("groups").update({group_name: name, description: text, group_icon: icon}).where("id", group_id);
    }

    async deleteFromWaiting(group_id: number, member_id: number) {
        await this.knex("groups_waitmembers").where({group_id: group_id, member_id: member_id}).del();
    }

    async sendGroupMessage(member_id: number, group_id: number, content: string) {
        await this.knex.insert({member_id: member_id, group_id: group_id, content: content}).into("groups_messages");
    }
}
