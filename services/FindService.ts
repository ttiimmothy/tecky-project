import {Knex} from "knex";
import {logger} from "../lib/logger";
import {User} from "../lib/model";

export class FindService {
    constructor(private knex: Knex) {}

	async getUser(userId:number){
		return await this.knex.select("*").from("users").where("id",userId);
	}
    async availableUser(user: User) {
        let friendList: Array<number> = await this.knex
            .select("friend_id")
            .from("friends")
            .where("user_id", user.id)
            .pluck("friend_id");

        let liked: Array<number> = await this.knex
            .select("target_user_id")
            .from("like_pages")
            .where("answer_user_id", user.id)
            .pluck("target_user_id");
        logger.debug(liked);

        let likedByOther: Array<number> = await this.knex
            .select("answer_user_id")
            .from("like_pages")
            .where("target_user_id", user.id)
            .pluck("answer_user_id");
        logger.debug(likedByOther);

        if (user.orientation === 9) {
            let result = await this.knex
                .select("users.id as userId", "*")
                .from("users")
                .whereNotIn("id", friendList)
                .whereNotIn("id", liked)
                .whereNotIn("id", likedByOther)
                .whereNot("users.id", user.id)
                .andWhere((event) => event.where({orientation: user.gender}).orWhere({orientation: 9}));
            logger.debug(JSON.stringify(result, null, 4));
            return result;
        } else {
            let result = await this.knex
                .select("users.id as userId", "*")
                .from("users")
                .whereNot("users.id", user.id)
                .whereNotIn("id", friendList)
                .whereNotIn("id", liked)
                .whereNotIn("id", likedByOther)
                .andWhere((event) =>
                    event
                        .where({
                            orientation: user.gender,
                            gender: user.orientation,
                        })
                        .orWhere({
                            gender: user.orientation,
                            orientation: 9,
                        })
                );
            logger.debug(JSON.stringify(result, null, 4));
            return result;
        }
    }

    async currentLocation(id: number, latitude: number, longitude: number) {
        await this.knex("users").update({latitude: latitude, longitude: longitude}).where("id", id);
    }

    async originalLocation(id: number) {
        return await this.knex.select("latitude", "longitude").from("users").where("id", id);
    }
}
