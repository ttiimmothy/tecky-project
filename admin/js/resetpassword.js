let wrongMessage = document.querySelector(".show-unMatch-password");
let firstPassword = document.querySelector(".password-input-first-password");
let password = document.querySelector(".password-input-password");
let resetPassword = document.querySelector("#reset-password-form");
let username = document.querySelector(".username");
let firstFullDisplay = document.querySelector(".first-full-display");
let secondFullDisplay = document.querySelector(".second-full-display");
let timeSecond = document.querySelector(".second");
let resetMessage = document.querySelector(".reset-message");

async function showUsername() {
    const search = new URLSearchParams(location.search);
    const uuid = search.get("uuid");
    const res = await fetch("/getUsernameResetPassword", {
        method: "put",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({uuid: uuid}),
    });
    const result = await res.json();
    username.innerText = /*html*/ `${result.username}`;
}

firstPassword.addEventListener("input", function () {
    wrongMessage.style = "color:#ff0000";
    if (firstPassword.value !== password.value) {
        wrongMessage.innerText = /*html*/ `Password is unmatched`;
    } else if (firstPassword.value === password.value) {
        wrongMessage.innerText = /*html*/ `Password is matched`;
        wrongMessage.style = "color:#0011ff";
    }
    if (!password.value || !firstPassword.value) {
        wrongMessage.innerText = /*html*/ ``;
    }
});

password.addEventListener("input", function () {
    wrongMessage.style = "color:#ff0000";
    if (firstPassword.value !== password.value) {
        wrongMessage.innerText = /*html*/ `Password is unmatched`;
    } else if (firstPassword.value === password.value) {
        wrongMessage.innerText = /*html*/ `Password is matched`;
        wrongMessage.style = "color:#0011ff";
    }
    if (!password.value || !firstPassword.value) {
        wrongMessage.innerText = /*html*/ ``;
    }
});

resetPassword.addEventListener("submit", async function (event) {
    event.preventDefault();
    const form = event.currentTarget;
    let user = {};
    user.name = username.innerText;
    user.password = form.password.value;

    let res = await fetch("/getAllUsers");
    const checkUsername = await res.json();

    for (let checkUser of checkUsername) {
        if (checkUser.username === user.name) {
            if (firstPassword.value === password.value && firstPassword.value) {
                firstFullDisplay.classList.remove("show-first-full-display");
                secondFullDisplay.classList.add("show-second-full-display");
                resetMessage.innerText = /*html*/ `${username.innerText}, your password is reset`;

                await fetch("/resetPassword", {
                    method: "put",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify(user),
                });

                changeTime();
            }
        }
    }
});

function changeTime() {
    let sec = 5;
    timeSecond.innerText = /*html*/ `${sec}`;
    let intervalID = setInterval(function () {
        if (sec > 0) {
            sec--;
            timeSecond.innerText = /*html*/ `${sec}`;
        } else {
            clearInterval(intervalID);
            window.location.href = "login.html";
        }
    }, 1000);
}

window.onload = async function () {
    firstFullDisplay.classList.add("show-first-full-display");
    await showUsername();
};
