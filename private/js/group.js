async function loadGroup() {
    const res = await fetch("/groupList");
    const result = await res.json();
    const checkRes = await fetch("/checkInGroup");
    const checkResult = await checkRes.json();
    const checkWaitRes = await fetch("/groupWaitInfoList");
    const checkWaitResult = await checkWaitRes.json();
    const search = document.querySelector(".search-text").value.toUpperCase();
    const searchLength = document.querySelector(".search-text").value.length;
    if (search === "") {
        for (let i = 0; i < result.length; i++) {
            document.querySelector(".group-list").innerHTML += /*html*/ `
			<a class="group-link">
				<div class="group-data">
					<div class="groupFlex">
						<div class="icon-load-${i} group-icon">
							<img class="group-icon-load" id="group-icon-${i}" class="" src="../uploads/${result[i].group_icon}">
						</div>
						<div class="group-name-message">
							<div class="group-name" index="${result[i].id}">${result[i].group_name}</div>
							<div class="group-message-loaning-2">${result[i].description}</div>
						</div>
					</div>
					<div class="join-button" index="${result[i].id}"></div>
				</div>
			</a>`;
            if (!result[i].group_icon) {
                document.querySelector(`#group-icon-${i}`).classList.add("none");
            } else {
                document.querySelector(`.icon-load-${i}`).classList.remove("group-icon");
            }

            let joinButtons = document.querySelectorAll(".join-button");
            let groupLinks = document.querySelectorAll(".group-link");
            if (checkResult.length > 0) {
                for (let i = 0; i < joinButtons.length; i++) {
                    for (let myGroup of checkResult) {
                        let groupIndex = parseInt(joinButtons[i].getAttribute("index"));
                        if (myGroup.group_id !== groupIndex) {
                            if (checkWaitResult.length > 0) {
                                for (let j = 0; j < checkWaitResult.length; j++) {
                                    if (parseInt(checkWaitResult[j].group_id) === groupIndex) {
                                        joinButtons[i].innerHTML = /*html*/ `<div class="wait-to-join">待批核</div>`;
                                        break;
                                    } else {
                                        joinButtons[i].innerHTML = /*html*/ `<div class="btn btn-success join" index=${groupIndex}>加入</div>`;
                                    }
                                }
                            } else {
                                joinButtons[i].innerHTML = /*html*/ `<div class="btn btn-success join" index=${groupIndex}>加入</div>`;
                            }
                        } else {
                            groupLinks[i].setAttribute("href", `groupchatpage.html?id=${groupIndex}`);
                            joinButtons[i].innerHTML = /*html*/ ``;
                            break;
                        }
                    }
                }
            } else {
                for (let i = 0; i < joinButtons.length; i++) {
                    let groupIndex = parseInt(joinButtons[i].getAttribute("index"));
                    if (checkWaitResult.length > 0) {
                        for (let j = 0; j < checkWaitResult.length; j++) {
                            if (parseInt(checkWaitResult[j].group_id) === groupIndex) {
                                joinButtons[i].innerHTML = /*html*/ `<div class="wait-to-join">待批核</div>`;
                                break;
                            } else {
                                joinButtons[i].innerHTML = /*html*/ `<div class="btn btn-success join" index=${groupIndex}>加入</div>`;
                            }
                        }
                    } else {
                        for (let i = 0; i < joinButtons.length; i++) {
                            joinButtons[i].innerHTML = /*html*/ `<div class="btn btn-success join" index=${groupIndex}>加入</div>`;
                        }
                    }
                }
            }
        }
    } else if (search !== "") {
        for (let i = 0; i < result.length; i++) {
            if (result[i].group_name.substr(0, searchLength).toUpperCase() === search) {
                document.querySelector(".group-list").innerHTML += /*html*/ `
				<a class="group-link">
					<div class="group-data">
						<div class="groupFlex">
							${result[i].group_icon?`<div class="group-icon-load-other"><img src = ../uploads/${result[i].group_icon}></div>`:'<div class="group-icon"></div>'}
							<div class="group-name-message">
								<div class="group-name" index="${result[i].id}">${result[i].group_name}</div>
								<div class="group-message-loaning-2">${result[i].description}</div>
							</div>
						</div>
						<div class="join-button" index="${result[i].id}"></div>
					</div>
				</a>`;

                let joinButtons = document.querySelectorAll(".join-button");
                let groupLinks = document.querySelectorAll(".group-link");
                if (checkResult.length > 0) {
                    for (let i = 0; i < joinButtons.length; i++) {
                        for (let myGroup of checkResult) {
                            let groupIndex = parseInt(joinButtons[i].getAttribute("index"));
                            if (myGroup.group_id !== groupIndex) {
                                joinButtons[i].innerHTML = /*html*/ `<div class="btn btn-success join">加入</div>`;
                            } else {
                                joinButtons[i].innerHTML = /*html*/ `<div class="join none"></div>`;
                                groupLinks[i].setAttribute("href", `groupchatpage.html?id=${groupIndex}`);
                                break;
                            }
                        }
                    }
                } else {
                    for (let i = 0; i < joinButtons.length; i++) {
                        let groupIndex = parseInt(joinButtons[i].getAttribute("index"));
                        if (checkWaitResult.length > 0) {
                            for (let j = 0; j < checkWaitResult.length; j++) {
                                if (parseInt(checkWaitResult[j].group_id) === groupIndex) {
                                    joinButtons[i].innerHTML = /*html*/ `<div class="wait-to-join">待批核</div>`;
                                    break;
                                } else {
                                    joinButtons[i].innerHTML = /*html*/ `<div class="btn btn-success join" index=${groupIndex}>加入</div>`;
                                }
                            }
                        } else {
                            for (let i = 0; i < joinButtons.length; i++) {
                                joinButtons[i].innerHTML = /*html*/ `<div class="btn btn-success join" index=${groupIndex}>加入</div>`;
                            }
                        }
                    }
                }
            }
        }
    }

    let joinButtons = document.querySelectorAll(".join");
    for (let i = 0; i < joinButtons.length; i++) {
        joinButtons[i].addEventListener("click", async function (event) {
            let index = parseInt(event.currentTarget.getAttribute("index"));

            await fetch("/joinGroup", {
                method: "post",
                headers: {
                    "Content-type": "application/json",
                },
                body: JSON.stringify({index: index}),
            });
            event.target.classList.remove("btn", "btn-success", "join");
            event.target.classList.add("wait-to-join");
            event.target.innerHTML = /*html*/ `待批核`;
        });
    }
}

document.querySelector(".search-text").addEventListener("keydown", function () {
    document.querySelector(".group-list").innerHTML = /*html*/ ``;
    loadGroup();
});

window.onload = function () {
    loadGroup();
};
