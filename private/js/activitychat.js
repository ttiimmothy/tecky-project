let urlParams = new URLSearchParams(window.location.search);
let activityId = urlParams.get("activityId");
let userId = urlParams.get("userId");

let socket;

function initSocket() {
    socket = io.connect();
}
initSocket();

socket.on("new-message", async function () {
    await getChatHistory();
    let bottom = document.querySelector(".chat-area");
    bottom.scrollTop = bottom.scrollHeight;
});

async function getProfile() {
    const res = await fetch("/showChatUsers/" + userId);
    const result = await res.json();
    const targetUser = result[0];
    document.querySelector(".user-data").innerHTML = /*html*/ ``;
    document.querySelector(".user-data").innerHTML += /*html*/ `
	<div class="user-icon"><img class="icon-XXsmall" src="../uploads/${targetUser.icon}"></div>
	<div class="user-name-message">
		<div class="user-name">${targetUser.display_name}</div>
		<div class="user-message-new">${targetUser.description}</div>
	</div>`;
}

async function getChatHistory() {
    const res = await fetch("/activityChat/" + activityId);
    const friendHistories = await res.json();
    document.querySelector(".chat-area").innerHTML = /*html*/ ``;
    let position;
    if (friendHistories.length > 0) {
        for await (let friendHistory of friendHistories) {
            if (parseInt(userId) === friendHistory.participant_id) {
                position = "receive";
            } else {
                position = "send";
            }
            const dateAndTime = new Date(friendHistory.created_at);
            let time =
                ("0" + (dateAndTime.getDate() + 1)).slice(-2) +
                "/" +
                ("0" + (dateAndTime.getMonth() + 1)).slice(-2) +
                " " +
                ("0" + dateAndTime.getHours()).slice(-2) +
                ":" +
                ("0" + dateAndTime.getMinutes()).slice(-2);

            document.querySelector(".chat-area").innerHTML += /*html*/ `
            <div class="user-data-chat-${position}">
                <div class="user-name-message user-chat-${position}">
					<div class="block-time-${position}">
						<div>${friendHistory.content}</div>
						<div class="message-time">${time}</div>
					</div>
                </div>
            </div>`;
            let bottom = document.querySelector(".chat-area");
            bottom.scrollTop = bottom.scrollHeight;
        }
    } else {
        document.querySelector(".chat-area").innerHTML = /*html*/ `
        <div class="empty-chat">
            <div>配對成功😍<div>
        <div>`;
    }
}

async function fetchMessage() {
    const message = {
        activity_id: activityId,
        content: document.querySelector(".text-input").value,
    };
    const res = await fetch("/newActivityMessage", {
        method: "post",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(message),
    });
    if (res.status === 200) {
        document.querySelector(".text-input").value = "";
    }
}

// add events to these 2 buttons
document.querySelector(".sendBtn").addEventListener("click", fetchMessage);
document.querySelector(".text-input").addEventListener("keyup", function (event) {
    if (event.keyCode === 13) {
        fetchMessage();
    }
});
window.onload = async function () {
    getProfile();
    getChatHistory();
};
