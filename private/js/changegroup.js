let urlParams = new URLSearchParams(window.location.search);
let urlID = urlParams.get("id");
let successMessage = document.querySelector(".error");
let groupName = document.querySelector(".groupName");
let groupText = document.querySelector(".groupText");
let uploadImageBtn = document.querySelector("#output_image");
let backToGroup = document.querySelector(".back-to-group");

function preview_image(event) {
    let reader = new FileReader();
    reader.onload = function () {
        let output = document.getElementById("output_image");
        output.src = reader.result;
    };
    reader.readAsDataURL(event.currentTarget.files[0]);
}
uploadImageBtn.addEventListener("click", function () {
    document.querySelector("#original-upload-btn").click();
});
backToGroup.setAttribute("href", `groupchatpage.html?id=${urlID}`);

async function checkInGroup() {
    const res = await fetch("/checkInSpecificGroup/" + urlID);
    const result = await res.json();
    return result;
}

async function loadGroupInfo() {
    const res = await fetch("/groupName/" + urlID);
    const result = await res.json();

    groupName.value = /*html*/ result.group_name;
    groupText.value = /*html*/ result.description;
    uploadImageBtn.src = /*html*/ `${result.group_icon ? `../uploads/${result.group_icon}` : ""}`;
}

async function checkAdmin() {
    const res = await fetch("/checkAdmin/" + urlID);
    const result = await res.json();
    return result.is_admin;
}

document.querySelector("#createGroup").addEventListener("submit", async (event) => {
    event.preventDefault();

    const form = event.currentTarget;
    const formData = new FormData();
    formData.append("group_name", form.groupName.value);
    formData.append("description", form.groupText.value);
    if (form.groupIcon.files[0]) {
        formData.append("group_icon", form.groupIcon.files[0]);
    }

    const res = await fetch("/changeGroup/" + urlID, {
        method: "put",
        body: formData, //formData唔需要Content-Type 果個header
    });

    if (res.status === 200) {
        successMessage.innerHTML = /*html*/ `<div class="sign-up-error">成功修改</div>`;
        form.reset();
        setTimeout(function () {
            window.location.href = `groupchatpage.html?id=${urlID}`;
        }, 1000);
    }
});

window.onload = async function () {
    const result = await checkInGroup();
    const admin = await checkAdmin();
    if (result.length > 0) {
        if (admin) {
            await loadGroupInfo();
        } else {
            window.location.href = `groupchatpage.html?id=${urlID}`;
        }
    } else {
        window.location.href = "group.html";
    }
};
