let urlParams = new URLSearchParams(window.location.search);
let urlID = urlParams.get("id");
// webkitURL is deprecated but nevertheless
let URL = window.URL || window.webkitURL;
let gumStream; // stream from getUserMedia()
let rec; // recorder.js object
let input; // MediaStreamAudioSourceNode we'll be recording
let AudioContext = window.AudioContext || window.webkitAudioContext;
let audioContext; // audio context to help us record
let recordButton = document.getElementById("recordButton");
let stopButton = document.getElementById("stopButton");
let successMessage = document.querySelector(".error");
let rightButton = document.querySelector(".rightBtn");

function goBack() {
    window.history.back();
}

async function getProfile() {
    const res = await fetch("/showUsers/" + urlID);
    const result = await res.json();
    const targetUser = result[0][0];
    let receivedRecording = result[1][0];
    let sentRecording = result[2][0];
    let targetUserGender;
    switch (targetUser.gender) {
        case 1:
            targetUserGender = /*html*/ `<i class="fas fa-mars"></i>`;
            break;
        case 2:
            targetUserGender = /*html*/ `<i class="fas fa-venus"></i>`;
            break;
        case 3:
            targetUserGender = /*html*/ `<i class="fas fa-transgender"></i>`;
            break;
    }

    document.querySelector(".icon").innerHTML = /*html*/ `
    <div class="like-icon-color">
		<div class="like-icon">
			<img class="icon-small" src="../uploads/${targetUser.icon}" />
		</div>
		<div class="like-message">
			<div class="icon-data">
				<div class="user-name">${targetUser.display_name}</div>
				<div class="user-profession">${targetUser.description}</div>
			</div>
			<div class="gender-age">
				<div>${targetUserGender}</div>
				<div>${2021 - new Date(targetUser.date_of_birth).getFullYear()}</div>
			</div>
		</div>
	</div>`;

    if (result[1].length > 0) {
        let emojiIcon;
        switch (receivedRecording.emoji) {
            case "Neutral":
                emojiIcon = /*html*/ `🙂`;
                break;
            case "Happy":
                emojiIcon = /*html*/ `😄`;
                break;
            case "Sad":
                emojiIcon = /*html*/ `😢`;
                break;
            case "Angry":
                emojiIcon = /*html*/ `😡`;
                break;
            case "Fearful":
                emojiIcon = /*html*/ `😰`;
                break;
            case "Disgusted":
                emojiIcon = /*html*/ `🤢`;
                break;
            case "Surprised":
                emojiIcon = /*html*/ `🤩`;
                break;
            default:
                emojiIcon = /*html*/ ``;
                break;
        }
        document.querySelector(".leftBtn").innerHTML = /*html*/ `
		<audio id="leftBtn" controls hidden src="../messages/${receivedRecording.recording}"></audio>
		<button onclick="document.getElementById('leftBtn').play()" class="playAudio">${emojiIcon}</button>`;
        rightButton.innerHTML = /*html*/ `
		<div class="smallRightSign">
			<i class="fas fa-check tickSign"></i>
			<i class="fas fa-times"></i>
		</div>`;
    }

    if (result[2].length > 0) {
        let emojiIcon;
        switch (sentRecording.emoji) {
            case "Neutral":
                emojiIcon = /*html*/ `🙂`;
                break;
            case "Happy":
                emojiIcon = /*html*/ `😄`;
                break;
            case "Sad":
                emojiIcon = /*html*/ `😢`;
                break;
            case "Angry":
                emojiIcon = /*html*/ `😡`;
                break;
            case "Fearful":
                emojiIcon = /*html*/ `😰`;
                break;
            case "Disgusted":
                emojiIcon = /*html*/ `🤢`;
                break;
            case "Surprised":
                emojiIcon = /*html*/ `🤩`;
                break;
            default:
                emojiIcon = /*html*/ ``;
                break;
        }
        document.querySelector(".leftBtn").innerHTML = /*html*/ `
		<audio id="leftBtn" controls hidden src="../messages/${sentRecording.recording}"></audio>
		<button class="playAudio">${emojiIcon}</button>`;
        rightButton.innerHTML = /*html*/ `
		<div class="playButton" onclick="document.getElementById('leftBtn').play()">
			<i class="fas fa-play"></i>
		</div>`;
    }
}

document.querySelector(".rejectUser").addEventListener("click", async () => {
    await fetch("/rejectUser/" + urlID, {method: "delete"});
    window.location.href = "find.html";
});

function startRecording() {
    /** simple constraints object, for more advanced audio features see
	https://addpipe.com/blog/audio-constraints-getusermedia/
	*/
    let constraints = {audio: true, video: false};

    // disable the record button until we get a success or fail from getUserMedia()
    recordButton.hidden = true;
    stopButton.hidden = false;

    /** we're using the standard promise based getUserMedia()
	https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
	*/
    navigator.mediaDevices.getUserMedia(constraints)
        .then(function (stream) {
            /** create an audio context after getUserMedia is called
			sampleRate might change after getUserMedia is called, like it does on macOS when recording through AirPods
			the sampleRate defaults to the one set in your OS for your playback device
			*/
            audioContext = new AudioContext();

            // assign to gumStream for later use
            gumStream = stream;

            // use the stream
            input = audioContext.createMediaStreamSource(stream);

            /** create the Recorder object and configure to record mono sound (1 channel)
			recording 2 channels will double the file size
			*/
            rec = new Recorder(input, {numChannels: 1});

            // start the recording process
            rec.record();
        })
        .catch(function () {
            // enable the record button if getUserMedia() fails
            recordButton.hidden = false;
            stopButton.hidden = true;
        });
}

function stopRecording() {
    // disable the stop button, enable the record too allow for new recordings
    stopButton.hidden = true;
    recordButton.hidden = false;

    // tell the recorder to stop the recording
    rec.stop();

    // stop microphone access
    gumStream.getAudioTracks()[0].stop();

    // create the wav blob and pass it on to createDownloadLink
    rec.exportWAV(createDownloadLink);
}

function createDownloadLink(blob) {
    let url = URL.createObjectURL(blob);
    let audio = document.createElement("audio");
    let div = document.createElement("div");
    let li = document.createElement("li");
    let link = document.createElement("a");

    // name of .wav file to use during upload and download (without extension)
    let filename = new Date().toISOString();

    // add controls to the <audio> element
    audio.controls = true;
    audio.src = url;

    // save to disk link
    link.href = url;
    link.download = filename + ".wav"; // download forces the browser to download the file using the  filename
    link.innerHTML = /*html*/ `Save to disk`;

    // add the new audio element to li
    li.appendChild(div);
    div.appendChild(audio);

    // upload link
    let upload = document.createElement("div");
    upload.innerHTML = /*html*/ `<button class="confirm-button"><i class="fas fa-check"></i></button>`;
    upload.addEventListener("click", async function () {
        let friend = new FormData();
        friend.append("content", blob, filename);
        const res = await fetch("/likeUser/" + urlID, {
            method: "post",
            body: friend,
        });
        const result = await res.json();
        successMessage.innerHTML = /*html*/ `<div class="sign-up-error">成功傳送</div>`;
        setTimeout(function () {
            window.location.href = "find.html";
        }, 500);
        await fetch("/sendEmojiRecording", {
            method: "post",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({message: result.message}),
        });
    });
    li.appendChild(document.createTextNode(" ")); // add a space in between
    li.appendChild(upload); // add the upload link to li

    //add the li element to the ol
    recordingsList.appendChild(li);
}

// add events to these 2 buttons
recordButton.addEventListener("click", () => {
    startRecording();
    setTimeout(() => {
        stopButton.addEventListener("click", stopRecording);
    }, 6000);
});

window.onload = function () {
    getProfile();
};
