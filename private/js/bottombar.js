function setBottomBar() {
    const bottomBar = document.querySelector("#bottom-bar");

    bottomBar.innerHTML = /*html*/ `
	<div>
		<a href="find.html">
			<div><i class="far fa-dot-circle"></i></div>
			<div>尋找</div>
		</a>
		</div>
		<div>
		<a href="invite.html">
			<div><i class="fas fa-grin-hearts"></i></div>
			<div>邀請</div>
		</a>
		</div>
		<div>
		<a href="explore.html">
			<div><i class="fas fa-globe"></i></div>
			<div>探索</div>
		</a>
		</div>
		<div>
		<a href="friendchat.html">
			<div><i class="fas fa-users"></i></div>
			<div>朋友</div>
		</a>
		</div>
		<div>
		<a href="profile.html">
			<div><i class="fas fa-address-card"></i></div>
			<div>用戶資料</div>
		</a>
	</div>`;
}

setBottomBar();
