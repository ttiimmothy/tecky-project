const displayBy = document.querySelectorAll(".display-by");
const displayByList = document.querySelector("#display-by-list");
const displayByIcon = document.querySelector("#display-by-icon");
const displayByLocation = document.querySelector("#display-by-location");
const mainContainer = document.querySelector(".main-container");
const iconWidth = 50;
const iconHeight = 50;
let availables;
let genderIcon;

function sortBy() {
    if (parseInt(displayByList.getAttribute("data-ascension")) === 1) {
        displayByListFunction();
    }
    if (parseInt(displayByIcon.getAttribute("data-ascension")) === 1) {
        displayByIconFunction();
    }
    if (parseInt(displayByLocation.getAttribute("data-ascension")) === 1) {
        displayByLocationFunction();
    }
}

function setSorting() {
    displayBy.forEach((element) => {
        element.addEventListener("click", () => {
            const removedArray = [...displayBy].filter((filtering) => {
                return filtering !== element;
            });
            removedArray.forEach((element) => {
                element.setAttribute("data-ascension", "0");
                element.classList.remove("active");
            });
            if (parseInt(element.getAttribute("data-ascension")) === 1) {
                element.setAttribute("data-ascension", "-1");
            } else {
                element.setAttribute("data-ascension", "1");
            }
            element.classList.add("active");
            sortBy();
        });
    });
}

async function showAvailable() {
    const res = await fetch("/showAvailable");
    availables = await res.json();
    sortBy();
    setSorting();
}

function displayByListFunction() {
    mainContainer.innerHTML = /*html*/ ``;

    for (let available of availables) {
        switch (available.gender) {
            case 1:
                genderIcon = /*html*/ `<i class="fas fa-mars"></i>`;
                break;
            case 2:
                genderIcon = /*html*/ `<i class="fas fa-venus"></i>`;
                break;
            case 3:
                genderIcon = /*html*/ `<i class="fas fa-transgender"></i>`;
                break;
        }

        mainContainer.innerHTML += /*html*/ `
		<a href="users.html?id=${available.userId}">
    		<div class="select-friend">
				<div>
					${available.icon?`<img class="icon-small" src="../uploads/${available.icon}"/>`:'<div class="icon-small"></div>'}
				</div>
				<div class="select-friend-info">
					<div class="display-name">${available.display_name}</div>
					<div class="description">${available.description}</div>
					<div class="gender-age">
						<div>${genderIcon}</div>
						<div>${2021 - new Date(available.date_of_birth).getFullYear()}</div>
					</div>
				</div>
			</div>
		</a>`;
    }
}

function displayByIconFunction() {
    mainContainer.innerHTML = /*html*/ `
    <div class="select-friend-container-icon">
	</div>`;

    for (let available of availables) {
        document.querySelector(".select-friend-container-icon").innerHTML += /*html*/ `
		<div>
			<a href="users.html?id=${available.userId}">
				${available.icon?`<img class="icon-medium find-icon-margin" src="../uploads/${available.icon}"/>`:'<div class="icon-medium find-icon-margin"></div>'}
			</a>
		</div>`;
    }
}

async function viewPeople() {
    const res = await fetch("/getOtherUsersLatLon");
    return await res.json();
}

async function setInfoWindows(map, markers) {
    for (let i = 0; i < markers.length; i++) {
        const res = await fetch("/getOtherUsersLatLon");
        const result = await res.json();
        markers[i].addListener("click", () => {
            const infoWindow = new google.maps.InfoWindow({
                content: /*html*/ `
				<div id="content-container">
					<a href="users.html?id=${result[i].id}">
						<div id="content-photo">
							${result[i].icon?`<img src="../uploads/${result[i].icon}" alt="icon" class="img-fluid">`:'<div class="img-fluid"></div>'}
						</div>
						<div class="info-flex">
							<div class="display-username">${result[i].display_name}</div>
							<div class="gender-age-other">
								<div>${2021 - new Date(result[i].date_of_birth).getFullYear()}</div>
							</div>
						</div>
					</a>
				</div>`,
            });
            infoWindow.open(map, markers[i]);
            map.panTo(markers[i].position);
        });
    }
}

function currentLocation(map) {
    const myLocationMarker = new google.maps.Marker({
        position: {lat: 22.302711, lng: 114.177216},
    });

    navigator.geolocation.getCurrentPosition(async (position) => {
        const pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
        };
        myLocationMarker.position = pos;
        map.panTo(myLocationMarker.position);
        google.maps.event.addListenerOnce(map, "bounds_changed", function () {
            map.setZoom(17.5);
        });
        await fetch("/currentLocation", {
            method: "post",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(pos),
        });
    });
}

async function myMap() {
    const markerIcon = {
        url: "/svgs/newlocationpin.svg", // url
        scaledSize: new google.maps.Size(iconWidth, iconHeight), // scaled size: width, height
        origin: new google.maps.Point(0, 0), // origin
        anchor: new google.maps.Point(iconWidth / 2, iconHeight), // anchor
    };

    let mapProp = {
        center: new google.maps.LatLng(22.323666483262922, 114.16861742758718),
        zoom: 12,
    };
    let map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

    let peopleLocations = await viewPeople(); // viewPeople function
    const markers = peopleLocations.map((location) => {
        return new google.maps.Marker({
            position: {lat: Number(location.latitude), lng: Number(location.longitude)},
            icon: markerIcon,
        });
    });

    new MarkerClusterer(map, markers, {
        imagePath: "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m",
    });

    setInfoWindows(map, markers); // setInfoWindows function
    currentLocation(map); // currentLocation function
}

function displayByLocationFunction() {
    mainContainer.innerHTML = /*html*/ `
	<div class="container map-container">
		<!-- Google Map -->
		<div id="googleMap" style="width:100%;height:500px;"></div>
	</div>`;
    myMap();
}

window.onload = async function () {
    await showAvailable();
};
