let successMessage = document.querySelector(".error");

document.querySelector("#new-dating").addEventListener("submit", async (event) => {
    event.preventDefault();

    const form = event.currentTarget;

    const newActivity = {};
    newActivity.activity_title = form.datingName.value;
    newActivity.activity_content = form.datingText.value;
    newActivity.activity_date = form.activityDate.value;
    newActivity.approach_gender = form.target.value;
    const res = await fetch("/newActivity", {
        method: "post",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(newActivity),
    });

    if (res.status === 200) {
        successMessage.innerHTML = /*html*/ `<div class="sign-up-error">成功發佈活動</div>`;
        form.reset();
        setTimeout(function () {
            window.location.href = "activity.html";
        }, 1000);
    }
});
