import {currentLocation} from "./updateLocation.js";

document.querySelector(".search-text").addEventListener("keydown", function () {
    document.querySelector(".main-container").innerHTML = /*html*/ ``;
    getProfile();
});

async function getProfile() {
    const res = await fetch("/getFriendList");
    const friendList = await res.json();
    const search = document.querySelector(".search-text").value.toUpperCase();
    const searchLength = document.querySelector(".search-text").value.length;

    if (search === "") {
        for (let i = 0; i < friendList.length; i++) {
            document.querySelector(".main-container").innerHTML += /*html*/ `
			<div class="group-list">
				<a href="friendchatpage.html?id=${friendList[i].id}">
					<div class="group-data">
						<div class="group-icon"><img class="icon-Xsmall" src="../uploads/${friendList[i].icon}"></div>
						<div class="group-name-message">
							<div class="group-name">${friendList[i].display_name}</div>
							<div class="group-message-new">${friendList[i].description}</div>
						</div>
					</div>
				</a>
			</div>`;
        }
    } else if (search !== "") {
        for (let i = 0; i < friendList.length; i++) {
            if (friendList[i].display_name.substr(0, searchLength).toUpperCase() === search) {
                document.querySelector(".main-container").innerHTML += /*html*/ `
				<div class="group-list">
					<a href="friendchatpage.html?id=${friendList[i].id}">
						<div class="group-data">
							<div class="group-icon"><img class="icon-Xsmall" src="../uploads/${friendList[i].icon}"></div>
							<div class="group-name-message">
								<div class="group-name">${friendList[i].display_name}</div>
								<div class="group-message-new">${friendList[i].description}</div>
							</div>
						</div>
					</a>
				</div>`;
            }
        }
    }
}

window.onload = async function () {
    await getProfile();
    currentLocation();
};
