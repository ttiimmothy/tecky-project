import express, {Request, Response} from "express";
import expressSession from "express-session";
import path from "path";
import http from "http";
import {Server as SocketIO} from "socket.io";
import dotenv from "dotenv";
import grant from "grant";
import {Socket} from "socket.io";
import {UserService} from "./services/UserService";
import {UserController} from "./controllers/UserController";
import {FindService} from "./services/FindService";
import {FindController} from "./controllers/FindController";
import {LikeController} from "./controllers/LikeController";
import {LikeService} from "./services/LikeService";
import {InviteController} from "./controllers/InviteController";
import {InviteService} from "./services/InviteService";
import {ActivityService} from "./services/ActivityService";
import {ActivityController} from "./controllers/ActivityController";
import {GroupService} from "./services/GroupService";
import {GroupController} from "./controllers/GroupController";
import {FriendService} from "./services/FriendService";
import {FriendController} from "./controllers/FriendController";
import {ProfileService} from "./services/ProfileService";
import {ProfileController} from "./controllers/ProfileController";
import {
    initRoute,
    userRoutes,
    findRoutes,
    likeRoutes,
    inviteRoutes,
    activityRoutes,
    groupRoutes,
    friendRoutes,
    profileRoutes,
} from "./routers/routes";
import Knex from "knex";
import {User} from "./lib/model";
import {isLoggedIn} from "./lib/guard";
const knexfile = require("./knexfile"); // 呢個位一定要寫require
const configEnv = "development";
export const knex = Knex(knexfile[configEnv]);
import {logger} from "./lib/logger";

// init express and socket io
// 提提你: Socket.io不是HTTP protocol, 亦唔係express嘅一部分, express係一個library
// http係node.js native嘅module去起server, 而express係基於http
export const app = express();
export const server = new http.Server(app);
export const io = new SocketIO(server);

// database
dotenv.config({path: __dirname + "./env/.env"});
logger.debug(path.resolve("./env/.env"));

// express session
const sessionMiddleware = expressSession({
    secret: "emojiandfriend",
    resave: true,
    saveUninitialized: true,
});
app.use(sessionMiddleware);
// socketio as express session
io.use((socket, next) => {
    const request = socket.request as express.Request;
    sessionMiddleware(request, request.res as express.Response, next as express.NextFunction);
});

// request body
// 要用req.body一定要有下面呢兩句
app.use(express.urlencoded({extended: true}));
app.use(express.json());

// grant social login
const grantExpress = grant.express({
    defaults: {
        origin: process.env.WEBSITE_LINK,
        transport: "session",
        state: true,
    },
    google: {
        key: process.env.GOOGLE_CLIENT_ID,
        secret: process.env.GOOGLE_CLIENT_SECRET,
        scope: ["profile", "email"],
        callback: "/login/google",
    },
    twitter: {
        key: process.env.TWITTER_CLIENT_ID,
        secret: process.env.TWITTER_CLIENT_SECRET,
        scope: ["profile", "email"],
        callback: "/login/twitter",
    },
    facebook: {
        key: process.env.FACEBOOK_CLIENT_ID,
        secret: process.env.FACEBOOK_CLIENT_SECRET,
        scope: ["openid", "email"],
        callback: "/login/facebook",
    },
});
app.use(grantExpress as express.RequestHandler);

// init io
io.on("connection", (socket: Socket) => {
    try {
        if (!(socket.request as express.Request).session["user"]) {
            socket.disconnect();
        }
        const user: User | undefined = (socket.request as express.Request).session["user"];
        if (user) {
            socket.join("userLogin");
            // One common way is to join the socket to a room named by the `user.id` or other group information.
        }
    } catch (e) {
        logger.error(e);
    }
});

// route.ts
export const userService = new UserService(knex);
export const userController = new UserController(userService);
export const findService = new FindService(knex);
export const findController = new FindController(findService);
export const likeService = new LikeService(knex);
export const likeController = new LikeController(likeService);
export const inviteService = new InviteService(knex);
export const inviteController = new InviteController(inviteService);
export const activityService = new ActivityService(knex);
export const activityController = new ActivityController(activityService, io);
export const groupService = new GroupService(knex);
export const groupController = new GroupController(groupService, io);
export const friendService = new FriendService(knex);
export const friendController = new FriendController(friendService, io);
export const profileService = new ProfileService(knex);
export const profileController = new ProfileController(profileService);

// initRoute()要寫得後過佢depends on嘅variable
initRoute();
// app.use((req, res, next) => {
//     logger.info(`path: ${req.path}, method: ${req.method}`);
//     next();
// });
app.use("/",userRoutes);
app.use("/",findRoutes);
app.use("/",likeRoutes);
app.use("/",inviteRoutes);
app.use("/",activityRoutes);
app.use("/",groupRoutes);
app.use("/",friendRoutes);
app.use("/",profileRoutes);

// go to folder to search files
app.use(express.static("public"));
app.use(express.static("public/html"));
app.use(isLoggedIn, express.static("private"));
app.use(isLoggedIn, express.static("private/html"));
app.use(express.static("admin"));
app.use(express.static("admin/html"));

// 404
app.use(function(req:Request,res:Response){
    res.sendFile(path.resolve("./public/html/404.html"));
});

// start listening to the route
server.listen(8000, function () {
    console.log("Listening at http://localhost:8000");
});
